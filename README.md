#  CI/CD system functionality 


This document is intended to familiarize the user with the functionality of the developed system.
The system consists of three classical CI/CD stages:
* publication;
* testing;
* delivery.


The system is activated every time a new commit is created in any branch or during a merge request to the "prod" branch. It is possible to roll back the changes made.

   
## <a name="Parag"></a>	Publication stage
At this stage, the environment described in the Dokerfile configuration file of the GitLab version control system is assembled on a dedicated server using Docker software and packages a file into it index.html . The security of accessing the system is provided by access only by a pre-generated token. 

## <a name="Parag"></a>	Testing stage
Testing is carried out by the static code analyzer SonarQube.The test result is displayed in it. To view it, you need to contact the 9000 port of the dedicated server. The security of accessing the system is provided by access only by a pre-generated token.  

## <a name="Parag"></a>	Delivery stage  
The stage of delivery is to place index.html on the server ip address, a separate port is used for each branch.This is achieved using the nginx web server. The security of accessing the system is provided by access only by an ssh key.
